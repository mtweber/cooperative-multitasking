#!/bin/sh --

set -efu

readonly BENCH_SIZE_CHASE=16000000   # ~2^24
readonly MODES='chase count'
readonly THRESHOLDS='5 10 100 1000 10000 100000 1000000 10000000 100000000 1000000000'
readonly NTHREADS='100 50 20 10 5 4 3 2'

# distro-specific commands:
CLANG=clang
OPT=opt
LLC=llc
distro="$(. /etc/os-release && echo "$ID")"
case "$distro" in
	(debian|ubuntu)
		CLANG="$CLANG-6.0"
		OPT="$OPT-6.0"
		LLC="$LLC-6.0"
		;;
	(arch)
		;;
	(*)
		printf 'Warning: Unknown distro, assuming CLANG=%s, OPT=%s, LLC=%s\n' \
		       "$CLANG" "$OPT" "$LLC" ;;
esac

rndcpuid() {
	echo $(($(seq "$(nproc --all)" | shuf | head -n1) - 1))
}

bench() {
	CLANG="$CLANG" \
	OPT="$OPT" \
	LLC="$LLC" \
	./bench.sh "$@"
}

run_configuration() {
	bench run "$@" "$(rndcpuid)" $nthreads $mode "$logdir"
}

test $# -gt 0 || { echo "Usage: $0 <logdir>"; exit 1; }

logdir="$1"
test -d "$logdir" || { echo "$logdir: No such directory"; exit 2; }
test -w "$logdir" || { echo "$logdir: No write permissions"; exit 3; }

# Build
for threshold in $THRESHOLDS; do
	bench build fiber "$threshold"
	bench build fiber_manual "$threshold"
done
bench build fiber_uninstrumented
bench build pthread
make CLANG="$CLANG" OPT="$OPT" LLC="$LLC" genchain

# Generate chain
case $MODES in (*chase*)
	bin/genchain $BENCH_SIZE_CHASE bin/chainfile
esac

# Run
for mode in $MODES; do
	for nthreads in $NTHREADS; do
		for threshold in $THRESHOLDS; do
			run_configuration fiber $threshold
			run_configuration fiber_manual $threshold
		done
		run_configuration fiber_uninstrumented
		run_configuration pthread
	done
done
