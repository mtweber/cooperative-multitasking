#!/bin/sh --

set -eu

readonly BENCH_CHAINFILE=bin/chainfile
readonly BENCH_SIZE_COUNT=500000000 # ~2^29
CFLAGS_LIBFIBER="-DARCH_x86_64 -m64 -Ilibfiber/include"
LIBS_LIBFIBER="-Llibfiber -lfiber"
LD_LIBRARY_PATH="libfiber:${LD_LIBRARY_PATH:-}"
#LIBFIBER_CFLAGS="$(pkgconf --cflags libfiber)"
#LIBFIBER_LIBS="$(pkgconf --libs libfiber)"
#LD_LIBRARY_PATH="${LD_LIBRARY_PATH:-}"

die_usage()
{
	if [ $# -gt 0 ]; then
		echo "$@" >&2
		echo
	fi

	cat <<- EOF
	Usage: $0 build <type> [<threshold>]
	          run <type> [<threshold>] <cpuid> <nthreads> <mode> <logdir>

	Types:
	  pthread
	  fiber
	  fiber_manual
	  fiber_uninstrumented

	Modes:
	  count
	  chase

	<threshold> only applicable for types fiber and fiber_manual
	EOF
	exit 1
}

die()
{
	echo "$@" >&2
	exit 1
}

is_nonnegative_numeric() {
	case "$1" in (*[!0-9]*)
		return 1
	esac
	test $1 -ge 0
}

readargs()
{
	# action:
	test $# -ge 1 || die_usage
	bench_action="$1"; shift
	case "$bench_action" in
		(build|run) ;;
		(*) die_usage "Unknown action: $bench_action"
	esac

	# type (and maybe threshold):
	test $# -ge 1 || die_usage
	bench_type="$1"; shift
	case "$bench_type" in
		(fiber|fiber_manual)
			test $# -ge 1 || die_usage 'Missing yield threshold'
			bench_threshold="$1"; shift
			is_nonnegative_numeric "$bench_threshold" \
				|| die "Yield threshold is not a non-negative numeric value: $bench_threshold"
			;;
		(fiber_uninstrumented|pthread) ;;
		(*) die_usage "Unknown benchmark type: $bench_type"
	esac

	if [ "$bench_action" = 'run' ]; then
		test $# -ge 1 || die_usage 'Missing CPU ID'
		bench_cpuid="$1"; shift
		is_nonnegative_numeric "$bench_cpuid" \
			|| die "CPU ID is not a non-negative numeric value: $bench_cpuid"

		test $# -ge 1 || die_usage 'Missing number of threads'
		bench_nthreads="$1"; shift
		is_nonnegative_numeric "$bench_nthreads" \
			|| die "Number of threads is not a positive numeric value: $bench_nthreads"

		test $# -ge 1 || die_usage 'Missing run mode'
		bench_mode="$1"; shift
		case "$bench_mode" in
			(count) bench_mode_arg=$BENCH_SIZE_COUNT ;;
			(chase) bench_mode_arg="$BENCH_CHAINFILE" ;;
			(*) die_usage "Unknown benchmark mode: $bench_mode" ;;
		esac

		test $# -ge 1 || die_usage 'Missing log directory'
		bench_logdir="$1"; shift
		test -d "$bench_logdir" || die "$bench_logdir: Directory not found"
		test -w "$bench_logdir" || die "$bench_logdir: Directory not writeable"
	fi

	test $# -eq 0 || die_usage "Trailing arguments: $*"
}

build()
{
	case "$bench_type" in
		(fiber|fiber_manual)
			make \
				CFLAGS_LIBFIBER="$CFLAGS_LIBFIBER" \
				LIBS_LIBFIBER="$LIBS_LIBFIBER" \
				YIELD_THRESHOLD="$bench_threshold" \
				CLANG="$CLANG" \
				OPT="$OPT" \
				LLC="$LLC" \
				"$bench_type"
			;;
		(fiber_uninstrumented)
			make \
				CFLAGS_LIBFIBER="$CFLAGS_LIBFIBER" \
				LIBS_LIBFIBER="$LIBS_LIBFIBER" \
				CLANG="$CLANG" \
				OPT="$OPT" \
				LLC="$LLC" \
				"$bench_type"
			;;
		(pthread)
			make \
				CLANG="$CLANG" \
				OPT="$OPT" \
				LLC="$LLC" \
				"$bench_type"
			;;
		(*)
			die "Unknown benchmark type: $bench_type"
	esac
}

run()
{
	case "$bench_type" in
		(fiber|fiber_manual)
			runname="${bench_type}_${bench_threshold}"
			;;
		(*)
			runname="$bench_type"
			;;
	esac
	logfile="$bench_logdir/$runname-$bench_nthreads-$bench_mode.txt"

	: >"$logfile"
	for i in 1 2 3; do
		echo "(nthreads=$bench_nthreads, cpuid=$bench_cpuid, mode=$bench_mode, mode_arg=$bench_mode_arg, #=$i) $runname"
		taskset -c $bench_cpuid \
		bin/"$runname" "$bench_nthreads" "$bench_mode" "$bench_mode_arg" \
		| tee -a "$logfile"
	done
}

readargs "$@"
$bench_action
