#define _ISOC11_SOURCE
#define _POSIX_C_SOURCE 199506L
#define _XOPEN_SOURCE 500

#include "bench.h"
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#if !defined(BENCH_FIBER) && !defined(BENCH_PTHREAD)
# define BENCH_FIBER
#endif

#if defined(BENCH_FIBER)
# include <fiber_event.h>
# include <fiber_manager.h>
int unsigned globalcount = 0;
#elif defined(BENCH_PTHREAD)
# include <pthread.h>
#endif

enum bench_mode {
	MODE_COUNT,
	MODE_CHASE,
};

struct bench_timer {
	struct timeval start;
	struct timeval end;
};

static void load_chain(char const *filename);
static int timeval_compare(struct timeval t1, struct timeval t2);
static struct timeval timeval_interval(struct timeval start, struct timeval end);
static void *bench_function(void *arg);
static void bench_init(size_t nthreads, enum bench_mode mode, size_t size,
                       char const *filename);
static void bench_print_times(void);
static void bench_term(void);
static void readargs(int argc, char **argv, size_t *nthreads,
                     enum bench_mode *mode, int unsigned *size,
                     char const **filename);

static struct {
	enum bench_mode mode;
	size_t nthreads;
	struct bench_timer *timers;

	/* counting */
	int long unsigned count_max;

	/* pointer-chasing */
	struct bench_link *chain;
	size_t chain_len;
	int chain_fd;
	char const *chain_filename;
} bench;

static void
load_chain(char const *filename)
{
	size_t fsize;
	struct stat st;

	/* file size */
	bench.chain_filename = filename;
	if (stat(bench.chain_filename, &st) < 0) {
		(void) fprintf(stderr, "Could not stat %s: %s\n",
		               bench.chain_filename, strerror(errno));
		goto error_stat;
	}
	fsize = (size_t) st.st_size;
	if (fsize % sizeof(struct bench_link) != 0) {
		(void) fprintf(stderr,
		               "Chain file %s is %zu bytes long (not a multiple of %zu bytes)\n",
		               bench.chain_filename, fsize,
		               sizeof(struct bench_link));
		goto error_size;
	}
	bench.chain_len = fsize / sizeof(struct bench_link);

	/* mmap file */
	bench.chain_fd = open(bench.chain_filename, O_RDONLY);
	if (bench.chain_fd < 0) {
		(void) fprintf(stderr, "Could not open %s for reading: %s\n",
		               bench.chain_filename, strerror(errno));
		goto error_open;
	}
	bench.chain = mmap(0, fsize, PROT_READ, MAP_PRIVATE, bench.chain_fd, 0);
	if (bench.chain == MAP_FAILED) {
		(void) fprintf(stderr, "Could not mmap %s: %s\n",
		               bench.chain_filename, strerror(errno));
		goto error_mmap;
	}
	return;

 error_mmap:
	close(bench.chain_fd);
 error_open:
 error_size:
 error_stat:
	exit(2);
}

static int
timeval_compare(struct timeval t1, struct timeval t2)
{
	return t1.tv_sec < t2.tv_sec ? -1
	     : t1.tv_sec > t2.tv_sec ? +1
	     : t1.tv_usec < t2.tv_usec ? -1
	     : t1.tv_usec > t2.tv_usec ? +1
	     : 0;
}

static struct timeval
timeval_interval(struct timeval start, struct timeval end)
{
	struct timeval interval;

	interval.tv_sec = end.tv_sec - start.tv_sec;
	interval.tv_usec = end.tv_usec - start.tv_usec;
	if (interval.tv_usec < 0) {
		interval.tv_usec += 1000000;
		interval.tv_sec -= 1;
	}

	return interval;
}

static void *
bench_function(void *arg)
{
	/* thread-specific data */
	int unsigned id = (int unsigned) (uintptr_t) arg;
	struct bench_timer *timer = &bench.timers[id];

	/* benchmark-specific data */
	int unsigned volatile value;
	struct bench_link volatile *l;
	int long unsigned i_start;

	/* run benchmark! */
	gettimeofday(&timer->start, NULL);
	switch (bench.mode) {
	case MODE_COUNT:
		for (value = 0ul; value < bench.count_max; ++value) {
#if defined(BENCH_FIBER) && defined(YIELD_THRESHOLD)
			if (++globalcount >= YIELD_THRESHOLD) {
				globalcount = 0;
				fiber_yield();
			}
#endif
		}
		break;
	case MODE_CHASE:
		i_start = id % bench.chain_len;
		l = &bench.chain[i_start];
		do {
			l = &bench.chain[l->i_next];
#if defined(BENCH_FIBER) && defined(YIELD_THRESHOLD)
			if (++globalcount >= YIELD_THRESHOLD) {
				globalcount = 0;
				fiber_yield();
			}
#endif
		} while (l != &bench.chain[i_start]);
		break;
	}
	gettimeofday(&timer->end, NULL);

	/* we don't care about the return value */
	return NULL;
}

static void
bench_init(size_t nthreads, enum bench_mode mode, size_t bench_size,
           char const *filename)
{
	bench.mode = mode;

	/* thread-specific data */
	bench.nthreads = nthreads;
	bench.timers = calloc(nthreads, sizeof(struct bench_timer));
	if (!bench.timers) {
		perror("calloc");
		exit(errno);
	}

	/* benchmark-specific data */
	switch (bench.mode) {
	case MODE_COUNT:
		bench.count_max = (int long unsigned) bench_size;
		break;
	case MODE_CHASE:
		load_chain(filename);
		break;
	}
}

static void
bench_print_times(void)
{
	int unsigned i;
	struct timeval earliest_start, latest_end, total_run;
	struct timeval longest_run, shortest_run;
	struct bench_timer t, reference;
	struct timeval trun;
	char const *esc_red, *esc_green, *esc_reset;

	if (isatty(1)) {
		esc_red = "\033[31m";
		esc_green = "\033[32m";
		esc_reset = "\033[0m";
	} else {
		esc_red = esc_green = esc_reset = "";
	}

	/* find time values */
	reference = bench.timers[0];
	earliest_start = reference.start;
	latest_end = reference.end;
	longest_run = timeval_interval(earliest_start, latest_end);
	shortest_run = longest_run;
	for (i = 0; i < bench.nthreads; ++i) {
		t = bench.timers[i];

		/* for total runtime */
		if (timeval_compare(t.start, earliest_start) < 0)
			earliest_start = t.start;
		if (timeval_compare(t.end, latest_end) > 0)
			latest_end = t.end;

		/* for individual times */
		trun = timeval_interval(t.start, t.end);
		if (timeval_compare(trun, longest_run) > 0)
			longest_run = trun;
		if (timeval_compare(trun, shortest_run) < 0)
			shortest_run = trun;
	}
	total_run = timeval_interval(earliest_start, latest_end);

	/* print values */
	(void) printf("%ld.%06ld %ld.%06ld %ld.%06ld\n",
	              total_run.tv_sec, total_run.tv_usec,
	              shortest_run.tv_sec, shortest_run.tv_usec,
	              longest_run.tv_sec, longest_run.tv_usec);
}

static void
bench_term(void)
{
	free(bench.timers);
	if (bench.mode == MODE_CHASE)
		close(bench.chain_fd);
}

static void
readargs(int argc, char **argv,
         size_t *nthreads, enum bench_mode *mode, int unsigned *size,
         char const **filename)
{
	int long nthreads_l, size_l;
	char *endptr;

	if (argc != 4) {
		(void) fprintf(stderr,
		               "Usage: %s {nthreads} count {size}\n"
		               "       %s {nthreads} chase {chainfile}\n",
		               argv[0], argv[0]);
		exit(1);
	}

	/* nthreads */
	errno = 0;
	nthreads_l = strtol(argv[1], &endptr, 10);
	if (errno != 0) {
		(void) perror("strtol");
		exit(1);
	}
	if (endptr == argv[1]) {
		(void) fprintf(stderr, "Not a numeric value: %s\n", argv[1]);
		exit(1);
	}
	if (nthreads_l < 0) {
		(void) fprintf(stderr, "Negative number of threads\n");
		exit(1);
	}
	*nthreads = (int long unsigned) nthreads_l;

	/* mode */
	if (strcmp(argv[2], "count") == 0) {
		*mode = MODE_COUNT;
	} else if (strcmp(argv[2], "chase") == 0) {
		*mode = MODE_CHASE;
	} else {
		(void) fprintf(stderr, "Valid modes are `count` and `chase`\n");
		exit(1);
	}

	switch (*mode) {
	case MODE_COUNT:
		/* size */
		errno = 0;
		size_l = strtol(argv[3], &endptr, 10);
		if (errno != 0) {
			(void) perror("strtol");
			exit(1);
		}
		if (endptr == argv[1]) {
			(void) fprintf(stderr, "Not a numeric value: %s\n", argv[1]);
			exit(1);
		}
		if (size_l < 0) {
			(void) fprintf(stderr, "Negative number for benchmark size\n");
			exit(1);
		}
		*size = (int unsigned) size_l;
		break;
	case MODE_CHASE:
		/* chain file */
		*filename = argv[3];
		break;
	}
}

int
main(int argc, char **argv)
{
	int unsigned i, size;
	size_t nthreads;
	enum bench_mode mode;
	char const *filename;

#if defined(BENCH_FIBER)
	fiber_manager_init(1);
#endif

	readargs(argc, argv, &nthreads, &mode, &size, &filename);

#if defined(BENCH_FIBER)
	fiber_t **threads = calloc(nthreads, sizeof(fiber_t *));
#elif defined(BENCH_PTHREAD)
	pthread_t *threads = calloc(nthreads, sizeof(pthread_t));
#endif

	bench_init(nthreads, mode, size, filename);

	for (i = 0; i < nthreads; ++i) {
#if defined(BENCH_FIBER)
		threads[i] = fiber_create(10240, bench_function,
		                          (void *) (uintptr_t) i);
		globalcount = 0;
#elif defined(BENCH_PTHREAD)
		/* XXX: pinning threads to a core is done using taskset */
		pthread_create(&threads[i], NULL, bench_function,
		               (void *) (uintptr_t) i);
#endif
	}
	
	for (i = 0; i < nthreads; ++i) {
#if defined(BENCH_FIBER)
		fiber_join(threads[i], NULL);
		globalcount = 0;
#elif defined(BENCH_PTHREAD)
		pthread_join(threads[i], NULL);
#endif
	}

	bench_print_times();
	bench_term();

	free(threads);
	return 0;
}
