#define _XOPEN_SOURCE 500

#include "bench.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define RSEED 20080704

static struct bench_link *generate_chain(size_t nelements);
static void readargs(int argc, char **argv, size_t *nelements,
                     char const **filename);
static void store_chain(struct bench_link *chain, size_t nelements,
                        char const *filename);

static struct bench_link *
generate_chain(size_t nelements)
{
	struct bench_link *chain;
	int long unsigned i_this, i_other, i_next, i_prev;
	struct bench_link *this, *other;

	chain = calloc(nelements, sizeof(struct bench_link));
	if (!chain) {
		(void) fprintf(stderr, "Could not allocate %zu bytes for chain of %zu elements: %s\n",
		               nelements * sizeof(struct bench_link),
		               nelements, strerror(errno));
		exit(2);
	}
	(void) fprintf(stderr, "Generating chain of %zu elements\n", nelements);

	/* simple chain */
	for (i_this = 0; i_this < nelements; ++i_this) {
		i_prev = ((int long unsigned) nelements
		         + i_this - 1) % (int long unsigned) nelements;
		i_next =  (i_this + 1) % (int long unsigned) nelements;
		chain[i_this].i_prev = i_prev;
		chain[i_this].i_next = i_next;
	}

	/* randomly move around links in the chain */
	srandom(RSEED);
	for (i_this = 0; i_this < nelements; ++i_this) {
		/* determine this and a random other link */
		this = &chain[i_this];
		do {
			for (i_other = 1;
			     i_other < nelements;
			     i_other *= ((int long unsigned) random() + 1));
			i_other %= nelements;
			other = &chain[i_other];
		} while (other == this);

		/* remove other link from chain */
		chain[other->i_prev].i_next = other->i_next;
		chain[other->i_next].i_prev = other->i_prev;

		/* insert into chain after this link */
		other->i_next = this->i_next;
		chain[other->i_next].i_prev = i_other;
		other->i_prev = i_this;
		this->i_next = i_other;
	}

	return chain;
}

static void
readargs(int argc, char **argv, size_t *nelements, char const **filename)
{
	int long nelements_l;
	char *endptr;

	if (argc != 3) {
		(void) fprintf(stderr, "Usage: %s {nelements} {filename}\n",
		               argv[0]);
		exit(1);
	}

	/* nelements */
	errno = 0;
	nelements_l = strtol(argv[1], &endptr, 10);
	if (errno != 0) {
		(void) perror("strtol");
		exit(1);
	}
	if (endptr == argv[1]) {
		(void) fprintf(stderr, "Not a numeric value: %s\n", argv[1]);
		exit(1);
	}
	if (nelements_l < 0) {
		(void) fprintf(stderr, "Negative number of threads\n");
		exit(1);
	}
	*nelements = (size_t) nelements_l;

	/* filename */
	*filename = argv[2];
}

static void
store_chain(struct bench_link *chain, size_t nelements, char const *filename)
{
	FILE *f;
	size_t nwritten;

	(void) fprintf(stderr, "Storing to %s (%zu bytes)\n",
	               filename, nelements * sizeof(struct bench_link));

	f = fopen(filename, "w");
	if (!f) {
		(void) fprintf(stderr, "Could not open %s for writing: %s",
		               filename, strerror(errno));
		exit(2);
	}
	nwritten = fwrite(chain, sizeof(struct bench_link), nelements, f);
	if (nwritten != nelements)
		(void) fprintf(stderr, "Could not write chain to %s: %s",
		               filename, strerror(errno));
	fclose(f);
}

int
main(int argc, char **argv)
{
	size_t nelements;
	char const *filename;
	struct bench_link *chain;

	readargs(argc, argv, &nelements, &filename);
	chain = generate_chain(nelements);
	store_chain(chain, nelements, filename);
}
