#include <llvm/Analysis/LoopInfo.h>
#include <llvm/Transforms/Utils/BasicBlockUtils.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/Support/CommandLine.h>
#include <llvm/Support/raw_ostream.h>
#include <cstring>

using namespace llvm;

static cl::opt<int unsigned> yield_threshold("yield-threshold",
                                             cl::desc("Define threshold for how long a fiber runs before yielding"),
                                             cl::value_desc("threshold"),
                                             cl::init(1024));

namespace {
	struct Yield : public ModulePass {
		static char ID; // Pass identification

		Yield(void) : ModulePass(ID) {};
		virtual bool runOnModule(Module &M);
		virtual void getAnalysisUsage(AnalysisUsage &AU) const;
		bool runOnFunction(Function &F);

	private:
		bool insertGlobalCountIncrement(BasicBlock &BB);
		bool insertYieldPrint(BasicBlock &BB);
		bool insertYield(BasicBlock &BB);
		bool insertVariableIncrement(BasicBlock &BB, StringRef varName);
		bool insertVariableReset(BasicBlock &BB, StringRef varName);
	};
}

bool
Yield::runOnModule(Module &M)
{
	bool modified = false;

	for (Function &F : M) {
		modified |= runOnFunction(F);
	}
	return modified;
}

void
Yield::getAnalysisUsage(AnalysisUsage &AU) const
{
	AU.addRequired<LoopInfoWrapperPass>();
}

bool
Yield::runOnFunction(Function &F)
{
	bool modified = false;

	if (!F.isIntrinsic() && !F.empty()) {
		LoopInfo &LI = getAnalysis<LoopInfoWrapperPass>(F).getLoopInfo();
		for (Loop *L : LI) {
			BasicBlock &BB = *L->getBlocks().back();
			errs() << "Running on function \033[1m" << F.getName() << "\033[0m, loop \033[32m" << L->getName() << "\033[0m\n";
			modified |= insertGlobalCountIncrement(BB);
		}
	}
	errs() << "Running on function \033[1m" << F.getName() << "\033[0m\n";
	if (F.getName().equals("fiber_yield")
	|| F.getName().equals("__errno_location")
	|| F.getName().equals("random")) {
		errs() << "-> Skipping (problematic function)\n";
	} else {
		modified |= insertGlobalCountIncrement(F.back());
	}
	return modified;
}

bool
Yield::insertGlobalCountIncrement(BasicBlock &BB)
{
	bool modified = false;

	Instruction *terminator = BB.getTerminator();
	if (!terminator) {
		return modified;
	}

	Module *module = BB.getModule();
	IRBuilder<> builder(terminator);

	// Increment the global counter
	BasicBlock *incBB = &BB;
	modified |= insertVariableIncrement(*incBB, "globalcount");

	// Create conditional block:
	Value *countPtr = module->getNamedValue("globalcount");
	Value *countVal = builder.CreateLoad(countPtr);
	ConstantInt *threshVal = builder.getInt32(yield_threshold);
	Value *cmpRes = builder.CreateICmpUGE(countVal, threshVal);
	BasicBlock *yieldBB = SplitBlockAndInsertIfThen(cmpRes, terminator, false)->getParent();
	modified |= insertVariableReset(*yieldBB, "globalcount");
	//modified |= insertYieldPrint(*yieldBB);
	modified |= insertYield(*yieldBB);

	return modified;
}

bool
Yield::insertYieldPrint(BasicBlock &BB)
{
	Instruction *terminator = BB.getTerminator();
	if (!terminator) {
		errs() << "\033[31mNo terminator for printing, ignoring\033[0m\n";
		return false;
	}

	LLVMContext &ctx = BB.getContext();
	Module *module = BB.getModule();
	IRBuilder<> builder(terminator);

	Value *counterPtr = module->getNamedValue("yieldcount");
	Value *counterVal = builder.CreateLoad(counterPtr);

	Constant *printfFunc = module->getOrInsertFunction(
		"printf",
		FunctionType::getVoidTy(ctx),
		FunctionType::getInt8PtrTy(ctx),
		FunctionType::getInt32Ty(ctx));
	Value *printStr = builder.CreateGlobalStringPtr("Yield %d!\n");
	std::vector<Value *> printfArgs;
	printfArgs.push_back(printStr);
	printfArgs.push_back(counterVal);
	builder.CreateCall(printfFunc, printfArgs);

	return true;
}

bool
Yield::insertYield(BasicBlock &BB)
{
	Instruction *terminator = BB.getTerminator();
	if (!terminator) {
		errs() << "\033[31mNo terminator for yielding, ignoring\033[0m\n";
		return false;
	}

	LLVMContext &ctx = BB.getContext();
	Module *module = BB.getModule();
	IRBuilder<> builder(terminator);

	Constant *yieldFunc = module->getOrInsertFunction(
		"fiber_yield",
		FunctionType::getVoidTy(ctx));
	builder.CreateCall(yieldFunc);

	return true;
}

bool
Yield::insertVariableIncrement(BasicBlock &BB, StringRef varName)
{
	Instruction *terminator = BB.getTerminator();
	if (!terminator) {
		errs() << "\033[31mNo terminator for incrementing `" << varName
		       << "`, ignoring\033[0m\n";
		return false;
	}

	Module *module = BB.getModule();
	IRBuilder<> builder(terminator);

	Value *varPtr = module->getNamedValue(varName);
	Value *varVal = builder.CreateLoad(varPtr);
	ConstantInt *incrVal = builder.getInt32(1);
	Value *sumVal = builder.CreateAdd(varVal, incrVal);
	builder.CreateStore(sumVal, varPtr);

	return true;
}

bool
Yield::insertVariableReset(BasicBlock &BB, StringRef varName)
{
	Instruction *terminator = BB.getTerminator();
	if (!terminator) {
		errs() << "\033[31mNo terminator for resetting `" << varName
		       << "` to zero, ignoring\033[0m\n";
		return false;
	}

	Module *module = BB.getModule();
	IRBuilder<> builder(terminator);

	Value *varPtr = module->getNamedValue(varName);
	ConstantInt *zeroVal = builder.getInt32(0);
	builder.CreateStore(zeroVal, varPtr);

	return true;
}

char Yield::ID = '\0'; // value doesn't matter, the address is used
static RegisterPass<Yield> X("yield", "ayekat's test pass", false, false);
