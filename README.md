Cooperative Multitasking
========================


LLVM Pass
---------

The [`llvm`](llvm) directory contains files for building an LLVM pass that
instruments code with yield instructions.

> #### Note
> Currently, it merely prints the first (LLVM) instruction in any basic block
> when processing the code; it does not modify the output program.

### Requirements

* clang
* cmake
* llvm
* make

### Build

To build the pass and run it on an example source file (`test.c`), run `make`
inside the `llvm` directory. This will perform the following steps:

(for the pass)

1. Create the `llvm/build` directory;
2. (with `cmake`) Generate a `build.ninja` file in the build directory;
3. (with `cmake`/`clang`) Build the pass residing in the `Yield` directory.

(for the test program)

1. (with `clang`) Transform the C code into LLVM bytecode (`.bc` file);
2. (with `opt`) Run the LLVM pass in `build/Yield` on the bytecode (generating a
   new bytecode file);
3. (with `llc`) Transform the second bytecode into an assembly file (`.s`);
4. (with `clang`) Compile the assembly file to a compiled object (`.o`);
5. (with `clang`) Link the object file to an executable.
