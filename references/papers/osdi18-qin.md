### How is context switching done?

7l, "Thread context":

> […] consists of its call stack, plus metadata […] Depending on how this
> information is managed […]

-> application-dependent? The paper does not seem to describe it.

* `src/Arachne.cc:361`: `swapcontext(void **saved, void **target)`
  Essentially the same as libfiber: push/pop r12, r13, r14, r15, rbx and rbp,
  and save/restore rsp to the context address (compare with
  `src/fiber_context.c:312`).

### How are "blocking" system calls handled?

The only file describing blocking system calls is `wiki/KernelAPI.md`, but it
does not match the given code (or paper), and the commit date is fairly far in
the past, so I suspect this was more of an "initial design idea" than really a
description of the code at hand.

### How do they handle large numbers of stacks?

Not explicitly mentioned, but the use of `maskAndCount` to represent which user
threads are currently in use on a given core (56-bit mask) limits the number of
threads that can be run on a single core.

> The `maskAndCount` variable limits Arachne to 56 threads per core at a given
> time. As a result, programming models that rely on large numbers of blocked
> threads may be unsuitable for use with Arachne.

<!--
Yeah, but… even those 56 threads could take a lot of space for their stacks
-->

* Just keeps a vector of stacks, with each stack assigned a predefined size.
  Neither the paper nor the code appear to mention anything about stack
  resizing, or treating them in any special way.

### How do handle threads that never yield?

* CoreArbiter sets bit in shared memory, and the Arachne dispatcher (scheduler)
  periodically checks the bit by calling `mustReleaseCore`, which returns true
  or false.

  If the application fails to release the core after 10ms, CoreArbiter reassigns
  the affected/blocking kernel thread to the unmanaged cpuset (which may affect
  its performance).

---

* 6l: priorities -> "divides the cores evenly"?
