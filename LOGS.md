October 4th to 6th:
* Trying to find out how to get the return value from a function call
* Attempted with a `StoreInst`, but apparently that expects a pointer

October 8th:
* Found http://llvm.org/docs/ProgrammersManual.html: Use IRBuilder

October 9th:
* Found https://stackoverflow.com/questions/22536097: Don't use TypeBuilder to
  describe function
