Writing a server application normally consists of reading data from a socket,
processing it, and responding to the client. One way to implement such software
is to create a separate thread per connection: each thread handles incoming data
for a single connection, and maintains only that connection's state. This
multithreaded approach is fairly intuitive; however, it does not scale
particularly well with a growing number of connections, as the kernel may not be
able to spawn tens of thousands of threads at once.

An alternative is to keep only a single thread that waits for events on all
sockets (\eg{}with \verb+epoll_wait+), and processes all incoming data within
the context of the corresponding connection. This scales a bit better than the
aforementioned multithreaded approach, as we do not spawn any new threads;
however, the experienced latency for individual connections may suffer: if one
connection misbehaves and keeps our single thread busy for a significant amount
of time, other connections are not handled within reasonable delays. This can be
improved by using \emph{thread pools}, \ie{}a fixed number of worker threads,
where the main thread can simply dispatch incoming data to the next available
thread, without risking to become blocked on problematic input data itself. This
ensures \emph{fairness} between connections; however, this also makes matters
more complex.  Furthermore, as threads will handle data for various connections,
connection states cannot be reflected in an intuitive way (simply by the
progress within the code), but must instead be encoded in and loaded from some
independent context data.

\subsection{Fibers}

% TODO: it would appear that the kernel can create up to 2^22 threads, which is
% IMHO a *lot*
The scalability issue stems from the fact that the kernel cannot reasonably
create any arbitray number of threads. We can avoid this limitation by not using
kernel threads, but \emph{fibers}~\cite{goodspeed:fibers}: a fiber is an
execution context that is created and maintained on top of a kernel thread,
typically by a userspace runtime. A fiber can be considered a ``userspace
thread'' insofar as it is associated an individual stack and can exist
independently from the fiber that created it; just like a thread, a fiber can
wait for and join with other fibers, or create new fibers on its own; and just
like threads, fibers rely on a separate component that manages and schedules
them.

As all the required metadata and stack memory is maintained in userspace,
context switching between fibers is a matter of saving a few registers on the
current fiber's stack, pointing the stack pointer to the new fiber's stack, then
restoring the registers from there, and jumping to the new fiber's current
execution position (also restored from the stack); this is less costly than
having to switch to kernel space to perform a full thread context switch.

As the kernel has no knowledge of fibers running on top of its threads, it
cannot preemptively interrupt and schedule fibers (it can only do so for entire
threads). Context switching to another fiber therefore requires that the fibers
cooperate: as long as a fiber does not yield on its own, the userspace scheduler
can not schedule another fiber. This is a major appeal of cooperative
multitasking: the lack of kernel preemption allows tighter control over the
execution flow of a multitasked program. However, it requires the programmer to
strategically place yields in their code to ensure that fibers do not prevent
each other from running, which can be tricky: if any execution follows a path in
the code that the programmer has not accounted for (and thus placed yields
insufficiently), this may result in a fiber blocking execution for other fibers.

Another issue is what happens when a fiber makes a blocking system call: ideally
another fiber could take over and continue running while the first one is
waiting for the system call to return. However, the kernel has no knowledge of
fibers; such a system call will simply block the entire kernel thread. Fibers
are therefore often written in a way that they only make non-blocking system
calls; however, this might impose a specific design choice on the program that
may not always be suitable.

Some fiber implementations work around this issue and provide mechanisms to
allow programmers to use blocking system calls with fibers. Anderson et al
proposed so-called \emph{scheduler activations}~\cite{anderson:sched-act},
\ie{}an extension to the kernel such that it can distinguish regular threads
from threads that run fibers and interact with userspace schedulers and
``activate'' them in specific events. Brian Watling's
\emph{libfiber}~\cite{watling:libfiber} on the other hand provides a drop-in
replacement wrapper (\emph{shim}) around blocking system calls; the shim calls
the respective \emph{non-blocking} variants, and then yields as long as the call
returns \verb+E_WOULDBLOCK+, and only returns once the system call returns a
real result, giving the fiber the illusion that it made a blocking system call.
A major advantage of this approach over scheduler activations is that this can
be done entirely in userspace and does not require any special support in the
kernel.

On the other hand, no fiber implementation has any mechanisms to guarantee
\emph{fairness} in cases where a fiber misbehaves and does not yield on its own.
One approach would be to use interprocess communication mechanisms like
interrupts or signals that trigger a routine in the userspace scheduler, which
preemptively switches to another fiber. However, we would lose all benefits of
having lightweight context switches between fibers if each context switch was
preceded by comparatively costly IPC involving the kernel.

For the sake of performance, we prefer to have fibers yield on their own, but
for the sake of robustness (and for practical reasons), we would like to relieve
the programmer from having to insert yields manually. Our proposal is thus a
mechanism that automatically inserts yields into the program at compile time.

\subsection{LLVM}

\llvm~\cite{lattner:llvm} is a compilation framework that handles various aspects
of compilation, including optimisation and code generation. As input, it
expects a specific \llvm-\emph{bitcode} (rather than code in a regular
programming language). The goal is to relieve language designers from having to
write a full blown compiler; instead, they only need to write an \llvm{}
\emph{frontend} for their language (lexer and parser), transform it to \llvm{}
bitcode, and have the \llvm{} \emph{backend} handle the rest of the compilation
procedure. A well-known \llvm{} frontend for the C language family is
Clang~\cite{clang}.

This modular design is also applied to the internals of the backend: \llvm{}
implements the various components involved in the compilation procedure---such
as optimisation---as so-called \emph{Passes} that each run on the code.
Third-party developers can add Passes themselves and thereby add further, more
specific functionality to \llvm{} if desired.

For our work, we intend to write an \llvm{} Pass that automatically injects
yields into the input code.

\subsection{Libfiber}

Libfiber~\cite{watling:libfiber} is a userspace library that provides an API
similar to \posix{} threads for creating and joining fibers and creating
mutexes, semaphores and spin locks, and provides a function \verb+fiber_yield+
that wraps around some fiber management operations and fiber context switches.

As it provides a familiar API, handles blocking I/O purely in userspace, and
does not pose any specific restrictions on the number of fibers that can be
created, we have decided to use libfiber as a basis for our work.
