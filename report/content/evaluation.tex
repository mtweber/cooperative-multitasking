The goal is to compare the cost of context switching between fibers with the
cost of context switching between kernel threads, but also see how much of an
overhead is added by maintaining an additional counter for determining when to
yield.

In this section, unless otherwise denoted, we refer to both userspace and kernel
threads as simply ``threads''.

\subsection{Setup}

We have written two simple throughput benchmarks (we measure the time between
the first thread starting and the last thread finishing):

\begin{itemize}
  \item \textbf{Counting}: Let each thread increment a thread-spe\-cif\-ic
    variable from 0 to some given, relatively high value.
  \item \textbf{Pointer Chasing}: Generate a list of elements that each contain
    pointers to their respective predecessors and successors, and store the
    elements in an array in a random order; then let each thread walk through
    that list until it ends up at its starting position. To avoid that threads
    ``profit'' from each others' memory reads by avoiding cache misses, each
    thread starts in a different position on the chain and---given the
    randomised nature of the chain in memory---should follow different paths.
\end{itemize}

Both benchmarks are compiled in multiple ways:

\begin{itemize}
  \item With libfiber, instrumented with our \llvm{} Pass and with yields
    inserted automatically;
  \item With libfiber, uninstrumented and with yields inserted manually;
  \item With libfiber, uninstrumented and without any yields;
  \item With \posix{} (kernel) threads.
\end{itemize}

For the variants that contain yield calls, we create multiple executables, one
for each threshold level among the following: $5$, $10$, $10^2$, $10^3$, $10^4$,
$10^5$, $10^6$, $10^7$, $10^8$ and $10^9$.

The executables are built without any compiler optimisations (\verb+-O0+). The
test system is an Intel~Core~i5-{}2500 CPU (second generation, ``Sandy Bridge'')
with 6144\,KiB cache, at 3.30\,GHz, and 8\,GiB of main memory, running Linux
4.20.3 on Arch Linux.

We also want to exclude effects on our measurements caused by true parallelism
(\ie{}threads running on multiple cores); we are purely interested in comparing
context switch times on a single CPU core. For the variants that use fibers we
therefore initialise libfiber with a single kernel thread, and for the
benchmarks using \posix{} threads we ``pin'' all kernel threads to a single core
with \verb+taskset+.

We run each variant and each threshold level (where applicable) with the
following parameters:

\begin{itemize}
  \item 2, 3, 4, 5, 10, 20, 50 and 100 threads;
  \item Counting from 0 to 500,000,000; and pointer chasing through
    16,000,000 elements à 16 bytes each (256\,MB).
\end{itemize}

We run each variant\@--parameter combination three times, and sum up the measured
running times.

\subsection{Expectation}

We expect that maintaining a yield counter adds some overhead, making our fiber
implementation slower than kernel threads in this regard; however, we also
expect context switching between fibers to take less time than switching between
kernel threads, making fibers faster.

Furthermore, since yielding itself adds an overhead to the execution time, we
expect the programs compiled with a higher yield threshold (thus yielding less
frequently) to perform better than programs with a lower threshold.
Consequently, we expect the extreme case (uninstrumented, without any yields) to
perform best of all, even better than kernel threads, as it does not perform any
more actions than the kernel threads, but also never context-switches during the
execution of a fiber.

We hope that the overhead added by maintaining a count\-er is outweighed by the
benefits of faster context switching, and that for some of the higher (but still
reasonably low) threshold values, the fibers perform better than kernel threads.
If not, we hope that fibers do not run too much slower than kernel threads (not
more than 5 or 10 percent added overhead).

\subsection{Results}

\subsubsection{Counting}

The execution times for 100 threads counting from 0 to 500,000,000 can be seen
in figure~\ref{fig:graph:100_count}. As expected, the execution time appears to
be inversely proportional to the yield threshold, although this relation remains
largely unnoticeable for thresholds from $10^9$ to $10^4$ (all with overheads of
around 5.2 to 8\,\% in comparison to kernel threads); execution times only start
rising noticeably when we get as low as 100 (37.8\,\% overhead), and rather
extremely with 10 (289\,\%) and 5 (580.2\,\%). We get very similar results for
any other number of threads.

\begin{figure}\centering
  \includegraphics[width=.45\textwidth]{figures/100_count}
  \caption{100 threads counting, with automatically inserted yields}%
  \label{fig:graph:100_count}
\end{figure}

\begin{figure}\centering
  \includegraphics[width=.45\textwidth]{figures/100_count_manual}
  \caption{100 threads counting, with manually inserted yields}%
  \label{fig:graph:100_count_manual}
\end{figure}

Unlike expected, the uninstrumented fiber variant (that neither counts nor
yields) does not run significantly faster than kernel threads. And more
confusingly, if we write the benchmark in a way that we manually count and yield
(instead of instrumenting the code with the \llvm{} Pass), the execution becomes
faster than both kernel threads and non-yielding fibers
(figure~\ref{fig:graph:100_count_manual}).

As the behaviour is otherwise rather similar to the instrumented variants (more
yields equals slower overall execution), this would indicate that our \llvm{}
Pass simply produces less optimised code. To see if compiler optimisation would
even out these differences, we built and ran the benchmark with \verb+-O3+
(figure~\ref{fig:graph:100_count_optimised}), and it turns out that compiler
optimisations do cause the instrumented and manual variants to give more similar
results.

There is an awkward detail, however: if we compare the absolute running times
for the manual variants, we observe that the optimised versions actually run
\emph{slower} than the non-optimised versions.

\begin{figure}\centering
  \includegraphics[width=.45\textwidth]{figures/100_count_optimised}
  \caption{100 threads counting, compiled with optimisations}%
  \label{fig:graph:100_count_optimised}
\end{figure}


\subsubsection{Pointer chasing}

The execution times for 100 threads chasing pointers over 16,000,000 elements
can be seen in figure~\ref{fig:graph:100_chase}. The results are \emph{wildly}
unexpected: for one, we get the lowest execution times with threshold values
around 100 (which completes in 70\,\% of the time taken by kernel threads),
whereas larger yield thresholds ($10^9$ to $10^6$) all result in roughly the
same execution times as the kernel threads. As expected, for very low yield
threshold values, the execution times go up again.

\begin{figure}\centering
  \includegraphics[width=.45\textwidth]{figures/100_chase}
  \caption{100 threads chasing pointers}%
  \label{fig:graph:100_chase}
\end{figure}

We have observed that neither changing the size of the chain nor changing the
number of threads changes this behaviour, with one exception: if run with only 2
threads, the results are as shown in figure~\ref{fig:graph:2_chase}. Compiling
with optimisations does not change this pattern.

\begin{figure}\centering
  \includegraphics[width=.45\textwidth]{figures/2_chase}
  \caption{2 threads chasing pointers}%
  \label{fig:graph:2_chase}
\end{figure}

Using the performance data obtained with linux-perf, we can compare the data for
the different variants (table~\ref{table:perf} shows a comparison between
thresholds 5 and $10^8$, for illustration). We observe that low-threshold
variants generally appear to have fewer L1 cache misses, which appears to result
in a significantly higher number of non-stalled instructions per cycle. The
lower cache miss rate for lower yield threshold values appears to indicate that
the chain is not sufficiently randomised and fibers thus follow too similar
paths, and we therefore get an effect of fibers profiting from each others
memory reads; however, the results do not change even after running on chains of
different sizes (both longer and shorter), nor if shuffled with various
different random seeds.

Unfortunately, linux-perf was unable to get the number of last-level cache (LLC)
misses from the CPU\@.

\begin{table}
  \begin{tabular}{lrr}
    \toprule
                     &    \textbf{thresh=5} & \textbf{thresh=10\textsuperscript{8}} \\
    \midrule
      cycles         &      349,047,740,311 &      372,017,103,625 \\
                     &   4,208,791.920\,GHz &   4,207,196.051\,GHz \\
    \addlinespace[0.75em]
      \dots\,stalled &      304,005,243,805 &      364,678,743,152 \\
      (frontend)     &            87.10\,\% &            98.03\,\% \\
    \addlinespace[0.75em]
      \dots\,stalled &      251,376,381,280 &      335,560,362,901 \\
      (backend)      &            72.02\,\% &            90.20\,\% \\
    \midrule
      instructions   &      152,960,803,893 &       32,000,778,333 \\
                     &            0.44\,IPC &            0.09\,IPC \\
      stalled        &            1.99\,CPI &           11.40\,CPI \\
    \midrule
      branches       &       32,000,182,047 &        3,200,177,626 \\
                     & 385,855,836.000\,M/s &  36,191,278.680\,M/s \\
    \addlinespace[0.75em]
      \dots\,missed  &            3,357,845 &                8,813 \\
                     &             0.01\,\% &             0.00\,\% \\
    \midrule
      L1-dcache      &       67,204,009,583 &       12,804,314,318 \\
      (loads)        & 810,340,993.127\,M/s & 144,805,870.782\,M/s \\
    \addlinespace[0.75em]
      L1-dcache      &        5,333,447,255 &        2,365,264,383 \\
      (misses)       &             7.94\,\% &            18.47\,\% \\
    \midrule
      LL cache       &        3,510,962,527 &        2,146,423,707 \\
      (loads)        &  42,334,927.315\,M/s &  24,274,220.879\,M/s \\
    \addlinespace[0.75em]
      LL cache       &                  --- &                  --- \\
      (misses)       &                      &                      \\
    \bottomrule
  \end{tabular}
  \caption{Comparison of linux-perf data for two variants of ``100 threads
           chasing pointers''}%
  \label{table:perf}
\end{table}
